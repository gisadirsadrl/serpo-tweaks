#modname "Commander Point Three"
#description "Reassigns vanilla commanders to cost 3 commander points according to the guidelines
0 < X < 3:  2CP
3 ≤ X ≤ 5:  3CP
5 < X < 10: 4CP
10 ≤ X:     5CP
where X is the total magic level plus 1/2 the holy level."
#version 0.03

--EA Arcoscephale
#selectmonster 1606
#rpcost 3
#end
#selectmonster 1070
#rpcost 1
#end
#selectmonster 311
#rpcost 3
#end

--EA Ermor
#selectmonster 1111
#rpcost 3
#end

--EA Marverni
#selectmonster 2468
#rpcost 3
#end

--EA Sauromatia
#selectmonster 1181
#rpcost 3
#end

--EA T'ien Ch'i
#selectmonster 940
#rpcost 3
#end

